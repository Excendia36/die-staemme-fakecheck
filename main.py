import PySimpleGUI as sg
import pyperclip

ds_troop_support_message = ""


def get_all_attacking_villages_list():
    file_content_list = ds_troop_support_message.split("\n")
    attacker_list = []
    for current_string in file_content_list:
        if "Ankunftszeit" in current_string:
            a = None
            b = None
            index = 0
            for character in current_string:
                if character == "|":
                    a = index-3
                    b = index+4
                    attacker_list.append(current_string[a:b])
                index += 1
    return attacker_list


def get_attacked_villages():
    return ds_troop_support_message.split("Dorf:")


def determine_attacked_village(current_attacked_village_string):
    a = current_attacked_village_string
    blist = a.split("\n")
    index = 0
    for character in blist[0]:
        if character == "|":
            a = index-3
            b = index+4
            return blist[0][a:b]
        index += 1
    return None


def get_attacked_villages_by_attacking_village_list(attacker_village_coord):
    attacked_list = []
    for current_string in get_attacked_villages():
        attacked_village_coord = determine_attacked_village(current_string)
        if attacker_village_coord in current_string and attacker_village_coord is not attacked_village_coord:
            attacked_list.append(attacked_village_coord)
    return attacked_list


def get_attacked_string_without_duplicates(get_attacked_villages):

    res = []
    [res.append(x) for x in get_attacked_villages if x not in res]
    attacked_string = ""
    index = 0
    for a in res:
        a = "[coord]" + a + "[/coord]"
        if index != len(res)-1:
            a += ", "
        attacked_string += a
        index += 1
    return attacked_string


def parse_final_message():
    attacker_list = get_all_attacking_villages_list()
    my_dict = {i: attacker_list.count(i) for i in attacker_list}
    final_message = ""
    for element in my_dict:
        final_message = final_message + "[coord]" + element + "[/coord]" + " = " + str(my_dict.get(
            element)) + " Angriff(e)   (Angegriffene Dörfer: " + get_attacked_string_without_duplicates(get_attacked_villages_by_attacking_village_list(element)) + ")\n"
    return final_message


def main():
    parse_final_message()


if __name__ == "__main__":
    main()


def open_window(final_message):
    layout = [[sg.Multiline(final_message, size=(100,10), key="textbox")],
            [sg.Button('Kopieren'), sg.Button('Schließen')]]
    window = sg.Window("Second Window", layout, modal=True)
    choice = None
    while True:
        event, values = window.read()
        if event == "Schließen" or event == sg.WIN_CLOSED:
            break
        if event == "Kopieren":
            pyperclip.copy(final_message)
            break

    window.close()


sg.theme('DarkAmber')   # Add a touch of color
# All the stuff inside your window.
layout = [[sg.Multiline(size=(60, 5), key='textbox')],
          [sg.Button('Nach fakes suchen'), sg.Button('Schließen')]]  # identify the multiline via key option

# Create the Window
window = sg.Window('DS-Fake Check', layout).Finalize()
# window.Maximize()
# Event Loop to process "events" and get the "values" of the inputs
while True:
    event, values = window.read()
    if event in (None, 'Schließen'):  # if user closes window or clicks cancel
        break
    else:
        # get the content of multiline via its unique key
        ds_troop_support_message = values['textbox']
        open_window(parse_final_message())
        #sg.Popup(parse_final_message(), keep_on_top=True, custom_text="Kopieren", title="Kopieren")

window.close()
